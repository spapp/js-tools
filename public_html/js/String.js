/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   js_tools
 * @since     2014.03.24.
 * @external  String
 */
/**
 * @constant
 * @type {number}
 */
String.PAD_LEFT = 0;
/**
 * @constant
 * @type {number}
 */
String.PAD_RIGHT = 1;
/**
 * @constant
 * @type {number}
 */
String.PAD_BOTH = 2;
/**
 * @constant
 * @type {string}
 * @default
 */
String.PAD_STRING = ' ';
/**
 * @constant
 * @type {number}
 * @default
 */
String.PAD_DIRECTION = String.PAD_RIGHT;
/**
 * Pad a string to a certain length with another string.
 *
 * <code>
 * var a = 'Alien';
 *
 * console.log(a.pad(10));              // returns 'Alien     '
 * console.log(a.padLeft(10, '-='));    // returns '-=-=-Alien'
 * console.log(a.padBoth(10, '_'));     // returns '__Alien___'
 * console.log(a.pad(6, '___'));        // returns 'Alien_'
 * </code>
 *
 * @link http://hu1.php.net/str_pad
 * @param {number} padLength
 * @param {string} [padString]
 * @param {number} [padType]
 * @returns {string}
 */
String.prototype.pad = function String_pad (padLength, padString, padType) {
    var methods = ['padLeft', 'padRight', 'padBoth'];

    if ('number' !== typeof(padType) || padType < 0 || padType > 2) {
        padType = String.PAD_DIRECTION;
    }

    return this[methods[padType]](padLength, padString);
};
/**
 * Pad a string to a certain length with another string.
 *
 * @see String.prototype.pad
 * @link http://hu1.php.net/str_pad
 *
 * @param {number|string} inputString
 * @param {number} padLength
 * @param {string} [padString]
 * @param {number} [padType]
 * @returns {string}
 */
String.pad = function String_static_pad (inputString, padLength, padString, padType) {
    // FIXME check type, NaN, Infinity
    // {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof#Examples}
    inputString = inputString + '';

    return inputString.pad(padLength, padString, padType);
};

(function () {
    /**
     * Returns a specal length string from other string.
     *
     * @param {number} padLength
     * @param {string} padString
     * @returns {string}
     * @private
     */
    function getPadText (padLength, padString) {
        padString = padString || String.PAD_STRING;
        var padText = '';

        while (padText.length < padLength) {
            padText = padString + padText;
        }

        if (padText.length > padLength) {
            padText = padText.slice(0, padLength - padText.length);
        }

        return padText;
    }

    /**
     * Returns text part.
     *
     * @param {string} text
     * @param {number} padLength
     * @returns {string}
     * @private
     */
    function getTextPart (text, padLength) {
        if (padLength < 0) {// FIXME see the direction
            return text.substr(padLength);
        }
        return text;
    }

    /**
     * Pad a string on the left side.
     *
     * Equal:
     * <code>
     * 'string'.pad(10, String.PAD_STRING, String.PAD_LEFT);
     * </code>
     *
     * @param {number} padLength
     * @param {string} padString
     * @returns {string}
     */
    String.prototype.padLeft = function String_padLeft (padLength, padString) {
        return getPadText(padLength - this.length, padString) + getTextPart(this, padLength);
    };
    /**
     * Pad a string on the right side.
     *
     * Equal:
     * <code>
     * 'string'.pad(10, String.PAD_STRING, String.PAD_RIGHT);
     * </code>
     *
     * @param {number} padLength
     * @param {string} padString
     * @returns {string}
     */
    String.prototype.padRight = function String_padRight (padLength, padString) {
        return getTextPart(this, padLength) + getPadText(padLength - this.length, padString);
    };
    /**
     * Pad a string on the both side.
     *
     * Equal:
     * <code>
     * 'string'.pad(10, String.PAD_STRING, String.PAD_BOTH);
     * </code>
     *
     * @param {number} padLength
     * @param {string} padString
     * @returns {string}
     */
    String.prototype.padBoth = function String_padBoth (padLength, padString) {
        var lenghLeft = parseInt(this.length / 2, 10);
        var lenghRight = this.length - lenghLeft;

        return getPadText(lenghLeft, padString) + getTextPart(this, padLength) + getPadText(lenghRight, padString);
    };
}());
/**
 * Make a string's first character uppercase.
 *
 * @link http://www.php.net/manual/en/function.ucfirst.php
 * @returns {string}
 */
String.prototype.ucfirst = function String_ucfirst () {
    return (this.substr(0, 1).toLocaleUpperCase() + this.substr(1));
};
/**
 * Make a string's first character uppercase.
 *
 * @link http://www.php.net/manual/en/function.ucfirst.php
 * @see String.prototype.ucfirst
 * @param {*} text
 * @returns {string}
 */
String.ucfirst = function String_static_ucfirst (text) {
    text = text + '';
    return text.ucfirst();
};

