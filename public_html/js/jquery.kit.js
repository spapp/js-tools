/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   js_tools
 * @since     2014.08.06.
 */
(function ($) {
    var CHARACTERS = 'abcdefghijklmnoprstuvz';
    var NUMBERS = '0123456789';
    var SPECIAL_CHARACTERS = '\'"+!%/=()~$,;.>-*_{}@&#<>|?';

    $.random = function jQuery_random (max, min) {
        min = min || 0;

        return min + Math.floor(Math.random() * max);
    };

    $.pwgen = function jQuery_pwgen (length, specialCharacters, lower, upper, numbers) {
        length = length || 8;

        var chars = '';
        var pw = '';

        if (false !== lower) {
            chars += CHARACTERS.toLowerCase();
        }

        if (false !== upper) {
            chars += CHARACTERS.toUpperCase();
        }

        if (false !== numbers) {
            chars += NUMBERS;
        }

        if (true === specialCharacters) {
            chars += SPECIAL_CHARACTERS;
        }

        while (length--) {
            pw += chars.charAt($.random(chars.length - 1));
        }

        return pw;
    };

    $.randomId = function jQuery_randomId (prefix, length) {
        var id = prefix || $.pwgen(1, false, true, true, false);
        length = length || 8;

        return (id + $.pwgen(length - id.length));
    };
})(jQuery);