/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   js_tools
 * @since     2014.08.06.
 */

QUnit.module('Date');

QUnit.test('now', function (assert) {
    assert.strictEqual(typeof Date.now, 'function');
    assert.strictEqual(Date.now(), (new Date()).getTime());
});

QUnit.test('isDate', function (assert) {
    assert.strictEqual(typeof Date.isDate, 'function');
    assert.strictEqual(Date.isDate(new Date()), true);
    assert.strictEqual(Date.isDate('2012-15-20'), false);
    assert.strictEqual(Date.isDate(123456789), false);
    assert.strictEqual(Date.isDate(true), false);
    assert.strictEqual(Date.isDate({}), false);
    assert.strictEqual(Date.isDate(null), false);
    assert.strictEqual(Date.isDate(undefined), false);
});

QUnit.test('format', function (assert) {
    var formatted = '2014-08-06 11:02:20';
    var d = new Date(2014, 7, 6, 11,2,20);

    assert.strictEqual(typeof Date.format, 'function');
    assert.strictEqual(typeof d.format, 'function');

    assert.strictEqual(d.format(Date.FORMAT_MYSQL_DATE_TIME), formatted);
    assert.strictEqual(Date.format(d, Date.FORMAT_MYSQL_DATE_TIME), formatted);

});

QUnit.test('create', function (assert) {
    var d = new Date(2014, 7, 6, 11,2,20);
    var now = new Date();

    assert.strictEqual(typeof Date.create, 'function');
    assert.strictEqual(Date.create().toString(), (new Date()).toString());
    assert.strictEqual(Date.create(2014, 7, 6, 11,2,20).toString(), (new Date(2014, 7, 6, 11,2,20)).toString());
    assert.strictEqual(Date.create(2014, 7, 6).toString(), (new Date(2014, 7, 6)).toString());
    assert.strictEqual(Date.create('2014-08-06').format('Y-M-D'), (new Date(2014, 7, 6)).format('Y-M-D'));
    assert.strictEqual(Date.create('11:02:20').toString(), new Date(d.getFullYear(), d.getMonth(), d.getDate(), 11,2,20).toString());
});

QUnit.test('setTranslate', function (assert) {
    var d = new Date(2014, 7, 6, 11,2,20);
    var format = 'YYYY. MMMM D., DDDD';
    var formatedEn = '2014. August 6., Wednesday';
    var formatedHu = '2014. Augusztus 6., Szerda';

    assert.strictEqual(typeof Date.setTranslate, 'function');
    assert.strictEqual(Date.format(d, format, Date.LOCALE_HU), formatedEn);

    Date.setTranslate(Date.LOCALE_HU, {August: 'Augusztus', Wednesday: 'Szerda'});

    assert.strictEqual(Date.format(d, format, Date.LOCALE_HU), formatedHu);
});


