/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   js_tools
 * @since     2014.08.06.
 */

QUnit.module('String');

QUnit.test('pad', function (assert) {
    var text = 'Alien';
    assert.strictEqual(typeof String.pad, 'function');

    assert.strictEqual(String.pad(text, 10), 'Alien     ');
    assert.strictEqual(String.pad(text, 10, '-=', String.PAD_LEFT), '-=-=-Alien');
    assert.strictEqual(String.pad(text, 10, '_', String.PAD_BOTH), '__Alien___');
    assert.strictEqual(String.pad(text, 6, '___'), 'Alien_');

    assert.strictEqual(typeof text.pad, 'function');

    assert.strictEqual(text.pad(10), 'Alien     ');
    assert.strictEqual(text.padLeft(10, '-='), '-=-=-Alien');
    assert.strictEqual(text.padBoth(10, '_'), '__Alien___');
    assert.strictEqual(text.pad(6, '___'), 'Alien_');
    assert.strictEqual(text.pad(10, String.PAD_STRING, String.PAD_RIGHT), text.padRight(10, String.PAD_STRING));

});

QUnit.test('ucfirst', function (assert) {
    var textA = 'alien';
    var textB = 'Alien';

    assert.strictEqual(typeof String.ucfirst, 'function');
    assert.strictEqual(String.ucfirst(textA), 'Alien');
    assert.strictEqual(String.ucfirst(textB), 'Alien');

    assert.strictEqual(typeof textA.ucfirst, 'function');
    assert.strictEqual(textA.ucfirst(), 'Alien');
    assert.strictEqual(textB.ucfirst(), 'Alien');
});

