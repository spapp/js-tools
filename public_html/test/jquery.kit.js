/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2014
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   js_tools
 * @since     2014.08.06.
 */

QUnit.module('jQuery.kit');

QUnit.test('random', function (assert) {
    var num;

    assert.strictEqual(typeof $.random, 'function');

    for (var i = 0; i < 1000; i++) {
        num = $.random(1000);

        assert.ok(num >= 0);
        assert.ok(num <= 1000);
    }
});

QUnit.test('pwgen', function (assert) {
    var i = 0;
    var count = 500;
    var pw;
    var length;

    assert.strictEqual(typeof $.pwgen, 'function');

    for (; i < count; i++) {
        length = $.random(20, 4);

        pw = $.pwgen(length);
        assert.strictEqual(pw.length, length);
    }

    function testPwgen (specialCharacters, lower, upper, numbers, regexp) {
        for (i = 0; i < count; i++) {
            pw = $.pwgen(8, specialCharacters, lower, upper, numbers);
            assert.strictEqual(pw.replace(regexp).length, 8);
        }
    }

    testPwgen(false, false, false, true, /\D/g);
    testPwgen(false, false, true, false, /[^A-Z]/g);
    testPwgen(false, true, false, false, /[^a-z]/g);
    testPwgen(true, false, false, false, /[a-z0-9]/gi);

    testPwgen(false, true, true, true, /[^a-z0-9]/gi);
});

QUnit.test('randomId', function (assert) {
    var i = 0;
    var count = 500;
    var length = 10;
    var prefix = 'id-';
    var prefixTest = new RegExp('^' + prefix);
    var id;

    assert.strictEqual(typeof $.randomId, 'function');

    for (; i < count; i++) {
        id = $.randomId(null, length);
        assert.strictEqual(/^[a-zA-Z]/.test(id), true);
        assert.strictEqual(id.length, length);
    }

    for (i = 0; i < count; i++) {
        id = $.randomId(prefix, length);
        assert.strictEqual(prefixTest.test(id), true);
        assert.strictEqual(id.length, length);
    }
});